import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { PortfolioViewDataService } from '../../services/portfolio-view-data.service';


@Component({
  selector: 'ws-portfolio-card',
  templateUrl: './portfolio-card.component.html',
  styleUrls: ['./portfolio-card.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class PortfolioCardComponent implements OnInit {

  constructor(private viewDataService: PortfolioViewDataService) {
  }

  ngOnInit() {
  }

}
