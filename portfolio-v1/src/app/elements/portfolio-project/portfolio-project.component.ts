import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { PortfolioViewDataService } from '../../services/portfolio-view-data.service';


@Component({
  selector: 'ws-portfolio-project',
  templateUrl: './portfolio-project.component.html',
  styleUrls: ['./portfolio-project.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class PortfolioProjectComponent implements OnInit {

  constructor(private viewDataService: PortfolioViewDataService) {
  }

  ngOnInit() {
  }

}
