import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'ws-project-page',
  templateUrl: './project-page.component.html',
  styleUrls: ['./project-page.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ProjectPageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
